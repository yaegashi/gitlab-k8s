# GitLab Kubernetes integration for AKS

Terraform files to deploy the followinng resources:

- Azure AD ([sp.tf](sp.tf))
  - Application for AKS
  - Service principal for AKS
  - Service principal password for AKS
- Azure RM ([aks.tf](aks.tf))
  - Resource group
  - AKS Cluster
- AKS Cluster ([k8s.tf](k8s.tf))
  - `gitlab-admin` service account
  - `gitlab-admin` role binding
  - `gitlab-admin` token data source
- GitLab ([gitlab.tf](gitlab.tf))
  - Personal project
  - Integration set up for an existing Kubernetes cluster

How to deploy them:

```console
$ git clone https://gitlab.com/yaegashi/gitlab-k8s
$ cd gitlab-k8s/terrafform/aks1
$ cp terraform.tfvars.sample terraform.tfvars
$ code terraform.tfvars
$ terraform init
$ terraform plan -var-file=terraform.tfvars
$ terraform apply  -var-file=terraform.tfvars
```

You need to prepare the following authentication beforehand:

- Log in with Azure CLI for Azure RM and Azure AD resources
  - Set `tenant_id` and `subscription_id`
- Make GitLab personal access token with `api` scope
  - Set `gitlab_token`
