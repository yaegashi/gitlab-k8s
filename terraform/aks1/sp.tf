provider azuread {
  tenant_id = var.tenant_id
  version   = "~> 0.7"
}

provider random {
  version = "~> 2.2"
}

resource "random_password" "gitlab_aks" {
  length = 32
}

resource "azuread_application" "gitlab_aks" {
  name = "gitlab-aks"
}

resource "azuread_service_principal" "gitlab_aks" {
  application_id = azuread_application.gitlab_aks.application_id
}

resource "azuread_service_principal_password" "gitlab_aks" {
  service_principal_id = azuread_service_principal.gitlab_aks.id
  value                = random_password.gitlab_aks.result
  end_date             = "2100-01-01T00:00:00Z"
}
