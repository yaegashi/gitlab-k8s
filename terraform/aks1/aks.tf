provider azurerm {
  tenant_id       = var.tenant_id
  subscription_id = var.subscription_id
  version         = "~> 1.44"
}

resource "azurerm_resource_group" "gitlab_aks" {
  name     = var.resource_group_name
  location = var.location
}

resource "azurerm_kubernetes_cluster" "gitlab_aks" {
  name                = var.cluster_name
  location            = azurerm_resource_group.gitlab_aks.location
  resource_group_name = azurerm_resource_group.gitlab_aks.name
  dns_prefix          = var.dns_prefix

  service_principal {
    client_id     = azuread_service_principal.gitlab_aks.application_id
    client_secret = azuread_service_principal_password.gitlab_aks.value
  }

  default_node_pool {
    name       = "agentpool"
    vm_size    = var.vm_size
    node_count = var.node_count
  }
}
