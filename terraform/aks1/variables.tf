variable tenant_id {}

variable subscription_id {}

variable location {
  default = "Japan East"
}

variable resource_group_name {
  default = "gitlab-aks"
}

variable cluster_name {
  default = "gitlab-aks"
}

variable dns_prefix {
  default = "gitlab-aks"
}

variable vm_size {
  default = "Standard_B2s"
}

variable node_count {
  default = 1
}

variable gitlab_token {}

variable gitlab_base_url {
  default = "https://gitlab.com/api/v4/"
}

variable gitlab_project {
  default = "gitlab-aks"
}