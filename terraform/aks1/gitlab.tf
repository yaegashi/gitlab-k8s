provider "gitlab" {
  token    = var.gitlab_token
  base_url = var.gitlab_base_url
  version  = "~> 2.5"
}

resource "gitlab_project" "project" {
  name = var.gitlab_project
}

resource "gitlab_project_cluster" "cluster" {
  project            = gitlab_project.project.id
  name               = var.cluster_name
  enabled            = true
  managed            = true
  kubernetes_api_url = azurerm_kubernetes_cluster.gitlab_aks.kube_config.0.host
  kubernetes_token   = data.kubernetes_secret.gitlab_admin.data.token
  kubernetes_ca_cert = base64decode(azurerm_kubernetes_cluster.gitlab_aks.kube_config.0.cluster_ca_certificate)
}
