provider "kubernetes" {
  load_config_file       = false
  host                   = azurerm_kubernetes_cluster.gitlab_aks.kube_config.0.host
  client_certificate     = base64decode(azurerm_kubernetes_cluster.gitlab_aks.kube_config.0.client_certificate)
  client_key             = base64decode(azurerm_kubernetes_cluster.gitlab_aks.kube_config.0.client_key)
  cluster_ca_certificate = base64decode(azurerm_kubernetes_cluster.gitlab_aks.kube_config.0.cluster_ca_certificate)
  version                = "~> 1.10" // https://github.com/terraform-providers/terraform-provider-kubernetes/issues/759
}

resource "kubernetes_cluster_role_binding" "gitlab_admin" {
  metadata {
    name = "gitlab-admin"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "gitlab-admin"
    namespace = "kube-system"
  }
}

resource "kubernetes_service_account" "gitlab_admin" {
  metadata {
    name      = "gitlab-admin"
    namespace = "kube-system"
  }
}

data "kubernetes_secret" "gitlab_admin" {
  metadata {
    name      = kubernetes_service_account.gitlab_admin.default_secret_name
    namespace = "kube-system"
  }
}
